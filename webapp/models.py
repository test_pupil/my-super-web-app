import datetime

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from webapp import db, login


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128), nullable=False)
    posts = db.relationship('Post', backref='user', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer,
                   primary_key=True)
    title = db.Column(db.String(64),
                         index=True,
                         unique=True,
                         nullable=False)
    description = db.Column(db.String(240))
    likes = db.Column(db.Integer, default=0)
    created = db.Column(
        db.DateTime, default=datetime.datetime.utcnow)
    comments = db.relationship('Comment', backref='post', lazy=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return f'Post "{self.title}"'


class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))

    def __repr__(self):
        return f'Comment "{self.title} by post {self.post_id}"'


@login.user_loader
def load_user(id):
    return User.query.get(int(id))