from flask import render_template
from flask import redirect
from flask import url_for
from flask_login import current_user, login_user, logout_user, login_required

from webapp import app
from webapp import db
from webapp.models import Post, User
from webapp.forms import PostForm, LoginForm, RegistrationForm


@app.route('/adding', methods=('GET', 'POST'))
@login_required
def adding():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data,
                    description=form.description.data)
        current_user.posts.append(post)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('home'))
    else:
        return render_template('adding.html', form=form)


@app.route('/')
@app.route('/home')
def home():
    posts = Post.query.all()
    return render_template('home.html', posts=posts)


@app.route('/post/<id>')
def post(id):
    post = Post.query.get(id)
    return render_template('post.html', post=post)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(
            username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            return render_template('login.html', form=form, error='Invalid login/password!')
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('home'))
    return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))
